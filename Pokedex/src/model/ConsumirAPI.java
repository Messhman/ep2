/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *
 * @author murilo
 */
public class ConsumirAPI {

    public static String[] lista_de_nomes = new String[949];
    public static ArrayList<Pokemon> pokemon = new ArrayList<>();

    public static void GETRequest() throws MalformedURLException, IOException {
        URL urlForGetRequest = new URL("https://pokeapi.co/api/v2/pokemon/");
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Falha : Código de Erro HTTP : " + connection.getResponseCode());
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String readLine = null, name = null, url_pokemon = null;
        while ((readLine = in.readLine()) != null) {
            if (readLine.contains("name")) {
                name = readLine.substring(readLine.indexOf(":") + 3, readLine.lastIndexOf("\""));
            }
            if (readLine.contains("url")) {
                url_pokemon = readLine.substring(readLine.indexOf(":") + 3, readLine.lastIndexOf("\""));
                pokemon.add(new Pokemon(name, url_pokemon));
            }
        }
        in.close();
        String capitalized;
        for (int i = 0; i < 949; i++) {
            capitalized = pokemon.get(i).getName().substring(0, 1).toUpperCase();
            lista_de_nomes[i] = capitalized + pokemon.get(i).getName().substring(1);
        }
    }
}
