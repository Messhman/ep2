# Pokedex
## Como usar o projeto

O projeto foi feito com a IDE Netbeans.

Para executar o projeto, baixe o .zip desse repositório e extraia no diretório de sua preferência.

Abra o Netbeans e vá em "Arquivo"->"Abrir Projeto...". Procure o diretório onde o arquivo extraído foi colocado. 

Entre na pasta e continue subindo até encontrar um arquivo nomeado "Pokedex" cujo ícone é uma xícara. 

Clique no arquivo "Pokedex" e então "Abrir Projeto". Para executar, clique em "Executar Projeto" (ícone de seta verde) ou pressione F6.

Alternativamente, há um arquivo nomeado "Pokedex.jar" na mesma pasta onde está o arquivo "Pokedex" citado acima. Executar "Pokedex.jar" iniciará o programa.

## Layout e passo a passo de utilização

### Tela inicial

![Image](Pokedex/Images/InterfaceListaPokemon.png)

**1. Retângulos verdes**

Lista de todos os pokémons disponíveis.

Inicialmente, o retângulo menor à direita (onde está o botão "Details") não estará visível. 

Clicar em qualquer pokémon da lista tornará visível o retângulo, apresentando a sprite do pokémon selecionado, seu(s) tipo(s) e o botão "Details". 

Clicar no botão "Details" criará a tela de detalhes do pokémon.

**2. Retângulo vermelho**

Menu do treinador.

Clicar no menu do treinador abrirá as opções de cadastrar, logar e listar os treinadores.

Clicar em "Register" criará a tela de cadastro de treinadores.

Clicar em "Login" criará a tela de login de treinadores.

Clicar em "List" criará a tela de listagem dos treinadores e seus respectivos pokémons.

**3. Retângulo azul escuro**

Busca por tipo.

Clicar na seta abrirá o dropbox para a seleção do tipo.

Selecionar um tipo automaticamente atualizará a lista de pokémons.

**4. Retângulo azul claro**

Busca por nome.

Digitar o nome de um pokémon ou quaisquer caracteres presentes no nome de um pokémon e então clicar no botão "Search" automaticamente atualizará a lista de pokémons.

Exemplo: digitar "ki" e clicar no botão "Search" atualizará a lista com todos os pokémons que possuirem a sílaba "ki" no nome.

**5. Retângulo rosa**

Botão de logout.

Caso algum treinador esteja logado, clicar no botão "Logout" realizará o logout da conta.

Logar em alguma conta atualizará o texto "Not logged." para "Currently logged as: nomedotreinador".

### Tela de detalhes do pokémon

![Image](Pokedex/Images/DetalhesPokemon.png)

Ao clicar no botão "Details" mencionado anteriormente, a tela de detalhes será apresentada.

Essa tela apresenta as características do pokémon.

"Stats" apresenta os atributos, tais como peso, velocidade e ataque.

"Abilities" apresenta as habilidades inatas do pokémon.

"Moves" apresenta os movimentos que o pokémon pode aprender.

Marcar a checkbox "Shiny" mudará a sprite do pokémon para seu tipo shiny.

Desmarcar a checkbox retornará a sprite para o tipo original.

Clicar no botão "Catch" adicionará o pokémon a lista de pokémons do treinador que estiver logado no momento.

### Telas de cadastro e login

As telas de cadastro e login são autoexplicativas.

Para o cadastro será requisitado um login e uma senha, que deve ser repetida para evitar erros.

Clicar no botão "Register" realizará o cadastro, exceto se o login digitado já estiver sendo utilizado por outra conta.

Para o login, será requisitado o login e a senha.

Clicar no botão "Login" realizará o login.

### Tela de lista

![Image](Pokedex/Images/Lista.png)

Clicar na seta abrirá o dropbox para selecionar o treinador.

Selecionar qualquer treinador atualizará a lista de pokémons para apresentar os pokémons do treinador selecionado.

Se o treinador não tiver pokémons, a lista estará em branco.

Ao abrir a tela de lista, a lista de pokémons estará em branco, mesmo que todos os treinadores cadastrados tenham algum pokémon.

É necessário selecionar o treinador no dropbox para atualizar a lista de pokémons.

Clicar no nome de algum pokémon na lista de pokémons criará a tela de detalhes do pokémon que já foi apresentada.

## Bugs

Às vezes uma jFrame vazia é criada ao utilizar a tela de registro, login ou lista.

Essa jFrame é criada com tamanho reduzido e portanto não atrapalha visualmente.

## Observações

É possível fechar qualquer uma das telas com o botão de sair padrão ("x" no canto superior direito nas imagens), exceto a tela inicial onde são listados todos os pokémons.

Fechar a tela inicial terminará o programa e fechará todas as outras telas automaticamente.

Clicar em algum pokémon da lista, clicar no botão "Details", entre algumas outras ações pode demorar alguns poucos segundos para executar.

Essas ações realizam requisições na API e, portanto, dependem da velocidade de conexão de internet.

As requisições foram feitas para solicitar apenas o mínimo necessário, reduzindo o tempo de espera.

Alguns pokémons não possuem sprite na API. Se algum desses pokémons for selecionado, a sprite será substituida por uma imagem que informa "No Image Avaiable".

## Referências

* https://pokeapi.co/api/v2/ --- API utilizada para a requisição de dados dos pokémons.

* https://stackoverflow.com/ --- Para tirar várias dúvidas pontuais.

* O aplicativo "dataDex - Pokédex para Pokémon" disponível na Play Store para consultar se os dados requisitados estavam corretos.
